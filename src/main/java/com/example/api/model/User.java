package com.example.api.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(updatable = false, nullable = false)
    private UUID id;

    private String nickname;

    private String username;

    private String surname;

    private String lastname;

    private String email;

    private String phone;


    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles;

    @Transient
    @Setter(AccessLevel.NONE)
    private List<Permission> permissions;

    public Collection<Permission> getPermissions() {
        if (permissions != null) {
            return permissions;
        }
        return getRoles().stream()
                .flatMap(it -> it.getPermissions().stream())
                .distinct()
                .collect(Collectors.toList());
    }
}
