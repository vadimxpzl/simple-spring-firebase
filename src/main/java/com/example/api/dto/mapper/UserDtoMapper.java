package com.example.api.dto.mapper;

import com.example.api.dto.model.UserDtoOut;
import com.example.api.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDtoMapper {
    UserDtoOut toDtoOut(User user);
}
