package com.example.api.dto.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDtoOut {
    private String username;
    private String surname;
    private String lastname;
    private String email;
}
