package com.example.api.service.user;

import com.example.api.model.User;

public interface UserService {

    User findByEmail(String email);
}
