package com.example.impl.controller;


import com.example.api.dto.mapper.UserDtoMapper;
import com.example.api.dto.model.UserDtoOut;
import com.example.impl.service.UserDetailsServiceImpl;
import com.example.impl.service.user.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(path = "/api/v1/user")
@Slf4j
public class UserController {

    private final UserServiceImpl userService;
    private final UserDtoMapper mapper;

    public UserController (UserServiceImpl userService, UserDtoMapper mapper){
        this.mapper = mapper;
        this.userService = userService;
    }

    @GetMapping("/details")
    public UserDtoOut getUserDetails(Principal principal){
        return  mapper.toDtoOut(userService.findByEmail(principal.getName()));
    }


}
