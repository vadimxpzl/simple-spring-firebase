create table users
(
    id         uuid   not null
        constraint users_pk
            primary key,
    nickname   varchar,
    username   varchar,
    surname    varchar,
    lastname   varchar,
    email      varchar,
    phone      varchar
);

alter table users
    owner to postgres;

create table role
(
    id   uuid not null primary key,
    name varchar
);

create table role_permissions
(
    role_id     uuid
        constraint role_permissions_role_id_fk references role (id),
    permissions integer
);

create table users_roles
(
    user_id uuid
        constraint users_roles_user_id_fk references users (id),
    roles_id uuid
        constraint users_roles_role_id_fk references role (id)
);
